# Variant of echo.s to be compiled with gcc.
.global main

.text
main:
        call read
	movq %rax, %r12
loop:
        cmpq $0, %r12
	je done
        call read
        pushq %rax
        call print
        addq $8, %rsp
        decq %r12
	jmp loop
done:
        movq $0, %rax    # exit code
	ret
