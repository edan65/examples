# Input: one line with 1 integer n, 0 <= n <= 100, the number of following lines.
# Then n lines with one integer each.
# Output all input except the first line.
.global _start

.text
_start:
        call read
	movq %rax, %r12
loop:
        cmpq $0, %r12
	je done
        call read
        pushq %rax
        call print
        addq $8, %rsp
        decq %r12
	jmp loop
done:
        movq $0, %rdi    # exit code
        movq $60, %rax   # sys_exit
        syscall
