# Helper code for using CodeProber

## Run CodeProber with logging turned on

We are grateful if you will run CodeProber with logging turned on. We will use the logging information for research purposes.

The script `cpr.sh` runs codeprober with logging turned on, generating log files into the dir `logs`.

Download [cpr.sh](cpr.sh), place it in the same directory as `code-prober.jar`, and make sure the file is executable (if not, do `chmod a+x cpr.sh`). Then, instead of writing, e.g.:
```
java -jar ../code-prober.jar compiler.jar
```
You can write:
```
../cpr.sh compiler.jar
```

After each lab, or more often if you like, add the new log files to your git repo:
```
git add '*.json'
```

To be able to use your log files for research purposes, we ask you to sign a [consent form](informed-consent.md). (The form will be available at the lab sessions).
