# Consent for participating in the research project CodeProber

## What this study is about
The study aims to find out how CodeProber is used during the labs of the compiler course (EDAN65).

## Your involvement in the study
Should you want to, you can opt-in to generate and upload data for the study.

You can generate data by starting CodeProber with the script [cpr.sh](cpr.sh).
The data will be written into json files in a directory called `logs`.

You upload the data into your repository with git. E.g `git add '*.json' && git commit -m "Message here" && git push`.

Note that CodeProber does not upload any data automatically, only the data you actively add and push in git is uploaded.
You are therefore able to inspect all data before pushing it to your repository.

## Your participation is voluntary
Participation in the study is voluntary.
We will only look at data in the repositories that have signed this form.

If you initially consent but later change your mind, then you can withdraw consent by contacting Anton Risberg Alaküla at anton.risberg_alakula@cs.lth.se.

## How we will use the logging data
The data will be used for research purposes.
We want to identify how much CodeProber is used for each lab, and what features are used.
We are also interested correlations between CodeProber usage and the rest of the code base.
There may be connections between how the compiler is implemented and how/if CodeProber is used.
By signing this consent form, you also consent to us analyzing the rest of your repository (for example .java, .jrag and .ast files) for the research study.

## Storage of personal information and logging data
The generated logs contain a list of JSON objects. Each object represents one action in CodeProber.
The actions do not contain any personal information such as your name, operating system, Java version, IP, etc.
Note however that names of your properties will be visible in the data.
Therefore, avoid calling your properties anything that can personally identify you.
For example, `type` is a good property name. `yourFirstnameAndLastname` is a very bad property name, and will be logged in the data.

If we make publications based on this data, we might make some of the data available publically.
If that happens, then we will anonymize the repository names as well.
Instead of `p123-abc-def` (where `abc-def` are your first name(s)), the repostiories will be called e.g `p123`.

## Your consent
I/We hereby consent to the CodeProber project to use our data in the following repository

`coursegit.cs.lth.se/edan65/2023/_____________________________`

I/We are aware that I / any of us may withdraw my/our consent at any time.

```


--------------------------               -----------------------------
Name                                     Name


--------------------------               -----------------------------
Date                                     Date


--------------------------               -----------------------------
Signature                                Signature
```
